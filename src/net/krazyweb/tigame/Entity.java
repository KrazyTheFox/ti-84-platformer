package net.krazyweb.tigame;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Entity {
	
	protected float x = 0;
	protected float y = 0;
	protected float lastX = 0;
	protected float lastY = 0;
	
	protected int texture;
	protected int mesh;
	
	protected int maxHealth;
	protected int health = maxHealth = 4; 
	
	protected long lastHit = 0;
	
	protected boolean destroy = false;
	
	protected Level level;

	protected float xBounce = 0;
	
	public Entity(float x, float y, Level level) {
		
		this.level = level;
		this.x = lastX = x;
		this.y = lastY = y;
		
		mesh = Main.renderer.beginMesh();
		
		int v1 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.25f, 0.25f));
		int v2 = Main.renderer.addVertexToMesh(new Vector3f(8.0f, 0.0f, 0.0f), new Vector2f(0.75f, 0.25f));
		int v3 = Main.renderer.addVertexToMesh(new Vector3f(8.0f, 8.0f, 0.0f), new Vector2f(0.75f, 0.75f));
		int v4 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 8.0f, 0.0f), new Vector2f(0.25f, 0.75f));
		
		Main.renderer.addTriangleToMesh(v1, v2, v3);
		Main.renderer.addTriangleToMesh(v1, v3, v4);
		
		Main.renderer.finishMesh();
		
	}
	
	public void update() {
		
		lastX = x;
		lastY = y;
		
	}
	
	public void render(double delta) {
		
		float actualX = (float) ((x * delta) + (lastX * (1.0 - delta)));
		float actualY = (float) ((y * delta) + (lastY * (1.0 - delta)));
		
		if (!Main.interpolation) {
			actualX = Math.round(actualX);
			actualY = Math.round(actualY);
		}
		
		if ((x < level.player.x - 96 || x > level.player.x + 96) || (y < level.player.y - 96 || y > level.player.y + 96)) {
			return;
		}
		
		Matrix4f output = Main.camera.orthoMatrix();
		Matrix4f.translate(new Vector3f(actualX, actualY, 0f), output, output);
		Main.renderer.setMatrix(output);
		
		Main.renderer.bindTexture(texture, 0);
		
		Main.renderer.drawMesh(mesh);
		
	}
	
	public void renderUI() {
		
	}
	
	public void delete() {
		Main.renderer.deleteMesh(mesh);
	}
	
	public void damage(int damage) {
		if (damage <= 0) {
			return;
		}
		health -= damage;
		if (health <= 0) {
			health = 0;
			new DeathAnimation(x, y, level);
			destroy = true;
		}
	}
	
	public void heal(int healAmount) {
		if ((health += healAmount) > maxHealth) {
			health = maxHealth;
		}
	}
	
}