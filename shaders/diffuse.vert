#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec4 Color;
layout (location = 2) in vec2 TexCoord;
layout (location = 3) in vec3 Normal;

uniform mat4 mvpMatrix;

out vec4 passColor;
out vec2 passCoords;
out vec3 passNormal;

void main()
{
    gl_Position = mvpMatrix * vec4(Position, 1.0);
    passColor = Color;
    passCoords = TexCoord;
    passNormal = Normal;
} 