package net.krazyweb.tigame;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Vector2f;

public class Skeleton extends Entity {
	
	private static int[] textures = new int[2];
	private static boolean loaded = false;

	private float xVelocity = 0;
	private float yVelocity = 0;
	private float moveSpeed = 0.55f;
	
	private boolean touchingGround = false;
	private boolean jumping = false;
	private boolean touchingHorizontal = false;
	
	public Skeleton(float x, float y, Level level) {
		
		super(x, y, level);
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("skeletonLeft.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("skeletonRight.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			loaded = true;
		}
		
		health = maxHealth = 6;
		
		texture = textures[0];
		
	}
	
	@Override
	public void update() {
		
		super.update();
		
		Vector2f dir = new Vector2f(level.player.x, level.player.y + 8);
		Vector2f.sub(dir, new Vector2f(x, y), dir);
		float distance = dir.length();
		
		if (dir.length() > 0) {
			dir.normalise();
		}
		
		if (distance < 120) {
			
			xVelocity = (dir.x > 0) ? moveSpeed : -moveSpeed;
			
			if (Math.round(x) == Math.round(level.player.x)) {
				xVelocity = 0;
			}
			
		} else {
			
			xVelocity = 0;
			yVelocity = 0;
			
		}
		
		if (xVelocity >= 0) {
			texture = textures[1];
		} else {
			texture = textures[0];
		}
		
		if (touchingGround) {
			jumping = false;
		}
		
		if (touchingHorizontal && !jumping) {
			yVelocity = 0.3f;
			jumping = true;
		} else {
			yVelocity -= 0.2f;
		}
		
		touchingHorizontal = false;
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x + xVelocity < actualBlockX + 8 && x + xVelocity + 8 > actualBlockX && y + 8 > actualBlockY && y < actualBlockY + 8) {
						
						if (x > actualBlockX) {
							x = actualBlockX + 8;
						} else if (x < actualBlockX) {
							x = actualBlockX - 8;
						}
						
						touchingHorizontal = true;
						
						xVelocity = 0;
						xBounce = 0;
						
					}
					
					if (x < actualBlockX + 8 && x + 8 > actualBlockX && y + yVelocity + 8 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						
						if (y > actualBlockY) {
							if (y + yVelocity <= actualBlockY + 8) {
								y = actualBlockY + 8;
							}
						}
						
						touchingGround = true;
						
						yVelocity = 0;
						
					}
					
				}
				
			}
		}
		
		x += xVelocity + xBounce;
		y += yVelocity;
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - 0.2f : xBounce + 0.2f;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (x < level.player.x + 8 && x + 8 > level.player.x && y + 8 > level.player.y && y < level.player.y + 8) {
			if (!level.player.invincible) {
				level.player.xBounce += (x > level.player.x) ? -level.player.knockbackAmount : level.player.knockbackAmount;
			}
			level.player.damage(1);
		}
		
	}
	
}
