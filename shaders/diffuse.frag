#version 330

in vec4 passColor;
in vec2 passCoords;
in vec3 passNormal;
in vec4 passPosition;

uniform sampler2D texture;
uniform vec4 diffuseColor;

layout(location = 0) out vec4 color;

void main( void )
{
    color = texture2D(texture, passCoords) * diffuseColor;
}