package net.krazyweb.tigame;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

public class HealthPowerup extends Powerup {
	
	private static int powerupTexture = 0;
	
	public HealthPowerup(float x, float y, Level level) {
		super(x, y, level);
		if (powerupTexture == 0) {
			powerupTexture = Main.renderer.loadTexture("healthPowerup.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		}
		texture = powerupTexture;
	}

	@Override
	public void performPowerup() {
		if (level.player.health == level.player.maxHealth && level.player.maxHealth < 15) {
			level.player.health = level.player.maxHealth = level.player.maxHealth + 1;
			level.updateText("Health Increased!");
		} else if (level.player.maxHealth == 15) {
			level.updateText("You have max HP!");
		} else {
			level.player.heal((int) Math.ceil((float) (level.player.maxHealth) / 3.0f));
			level.updateText("Restored Health!");
		}
	}
	
}
