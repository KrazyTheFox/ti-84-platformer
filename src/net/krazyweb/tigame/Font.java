package net.krazyweb.tigame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import net.krazyweb.renderer.Color;
import net.krazyweb.renderer.Renderer;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Font {
	
	private class Char {
		
		//protected char id;
		
		protected float textureX;
		protected float textureY;
		
		protected float width;
		protected float height;
		
		protected float xOffset;
		protected float yOffset;
		
		protected float xAdvance;
		
		public Char(int id, float texX, float texY, float w, float h, float xOff, float yOff, float xAdv) {
			//this.id = (char) (id);
			textureX = texX;
			textureY = texY;
			width = w;
			height = h;
			xOffset = xOff;
			yOffset = yOff;
			xAdvance = xAdv;
		}
		
	}
	
	private HashMap<Character, Char> characters = new HashMap<Character, Char>();
	private ArrayList<Integer> meshes = new ArrayList<Integer>();
	
	private int texture;
	private int texSize;
	
	private int lineHeight;
	
	private Renderer renderer;
	
	public Font(String fontName, Renderer renderer) {
		
		this.renderer = renderer;
		
		Scanner input = new Scanner(Font.class.getClassLoader().getResourceAsStream(fontName));
		
		int id = 0;
		float texX = 0, texY = 0, w = 0, h = 0, xOff = 0, yOff = 0, xAdv = 0;
		
		while (input.hasNextLine()) {
			
			String[] line = input.nextLine().split("\\s+");
			
			if (line[0].equals("common")) {
				
				for (String s : line) {
					switch (s.split("=")[0]) {
					
						case "lineHeight":
							lineHeight = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "scaleW":
							texSize = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "file":
							String texFile = s.split("=")[1];
							texFile = texFile.substring(1, texFile.length() - 1);
							texture = renderer.loadTexture(texFile, TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
							break;
							
					}
				}
				
			} else if (line[0].equals("char")) {
				
				for (String s : line) {
					switch (s.split("=")[0]) {
					
						case "id":
							id = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "x":
							texX = (float) (Integer.parseInt(s.split("=")[1])) / (float) texSize;
							break;
							
						case "y":
							texY = (float) (Integer.parseInt(s.split("=")[1])) / (float) texSize;
							break;
							
						case "width":
							w = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "height":
							h = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "xoffset":
							xOff = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "yoffset":
							yOff = Integer.parseInt(s.split("=")[1]);
							break;
							
						case "xadvance":
							xAdv = Integer.parseInt(s.split("=")[1]);
							break;
							
					}
				}
				
				characters.put((char) (id), new Char(id, texX, texY, w, h, xOff, yOff, xAdv));
				
			}
			
		}
		
		input.close();
		
	}
	
	public int createVBOFromString(String text) {
		
		int mesh = renderer.beginMesh();
		int currentWidth = 0;
		
		int v1 = 0, v2 = 0, v3 = 0, v4 = 0;
		
		for (char c : text.toCharArray()) {
			
			Char tempChar = characters.get(c);
			
			currentWidth += tempChar.xOffset;
			float y = lineHeight - tempChar.yOffset - tempChar.height;
			
			v1 = renderer.addVertexToMesh(new Vector3f(currentWidth, y, 0f), new Vector2f(tempChar.textureX, tempChar.textureY + (float) (tempChar.height / texSize)));
			v2 = renderer.addVertexToMesh(new Vector3f(currentWidth + tempChar.width, y, 0f), new Vector2f(tempChar.textureX + (float) (tempChar.width / texSize), tempChar.textureY + (float) (tempChar.height / texSize)));
			v3 = renderer.addVertexToMesh(new Vector3f(currentWidth + tempChar.width, y + tempChar.height, 0f), new Vector2f(tempChar.textureX + (float) (tempChar.width / texSize), tempChar.textureY));
			v4 = renderer.addVertexToMesh(new Vector3f(currentWidth, y + tempChar.height, 0f), new Vector2f(tempChar.textureX, tempChar.textureY));
			
			renderer.addTriangleToMesh(v1, v2, v3);
			renderer.addTriangleToMesh(v1, v3, v4);
			
			currentWidth += tempChar.xAdvance;
			
		}
		
		renderer.finishMesh();
		
		meshes.add(mesh);
		return mesh;
		
	}
	
	public void deleteFBO(int mesh) {
		renderer.deleteMesh(mesh);
	}
	
	public void render(double delta, float x, float y, float z, int mesh, int brightness) {
		
		Color color = null;
		
		if (brightness == 0) {
			color = Color.BLACK;
		} else if (brightness == 1) {
			color = new Color(76, 92, 59, 255);
		} else if (brightness == 2) {
			color = new Color(160, 176, 143, 255);
		} else {
			color = new Color(239, 255, 222, 255);
		}
		
		Matrix4f output = Main.camera.orthoMatrix();
		Matrix4f.translate(new Vector3f(x, y, z), output, output);
		Main.renderer.setMatrix(output);
		
		Main.renderer.setShaderProperty("diffuseColor", color);
		
		Main.renderer.bindTexture(texture, 0);
		
		Main.renderer.drawMesh(mesh);
		
	}
	
}
