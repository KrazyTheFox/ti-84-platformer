package net.krazyweb.tigame;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import net.krazyweb.renderer.OtherMath;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Level {
	
	private static final int SEGMENT_SIZE = 10;
	
	protected Player player;
	
	protected ArrayList<Entity> entities = new ArrayList<Entity>();
	protected ArrayList<Entity> toRemove = new ArrayList<Entity>();
	protected ArrayList<Entity> toAdd = new ArrayList<Entity>();
	
	private int text;
	private int textBack;
	private int textBackTexture;
	private long lastTextUpdate = 0;
	private int textBrightness = 0;
	
	public static enum BLOCK {
		
		GRASS(0),
		DARKENING_GRASS(1),
		DARK_GRASS(2),
		NONE(15);
		
		public int id;
		
		private BLOCK(int blockID) {
			this.id = blockID;
		}
		
	}
	
	public BLOCK[][] blocks;
	public int width = 100;
	public int height = 5;
	
	private int blockTextures;
	private int[] meshes;
	
	public Level(int blockTextures) {
		
		this.blockTextures = blockTextures;
		textBack = Main.renderer.getQuad(0, 0, 0, Main.WIDTH, 9);
		textBackTexture = Main.renderer.loadTexture("blackTexture.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		
		try {
			createLevel();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	private void createLevel() throws IOException {
		
		player = new Player(0, 6 * 8, this);
		
		BufferedImage levelImage = ImageIO.read(Level.class.getClassLoader().getResourceAsStream("Level1.png"));
		
		width = levelImage.getWidth();
		height = levelImage.getHeight();
		
		if (width % SEGMENT_SIZE != 0) {
			System.err.println("Map size must be a multiple of " + SEGMENT_SIZE + "!");
			System.exit(-1);
		}
		
		meshes = new int[width / SEGMENT_SIZE];
		
		blocks = new BLOCK[width][height];
		
		int rgb, r, g, b, a, realX, realY;
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				
				realX = x * 8;
				realY = y * 8;
				
				rgb = levelImage.getRGB(x, height - y - 1);
				a = (rgb >> 24) & 0xFF;
				r = (rgb >> 16) & 0xFF;
				g = (rgb >>8 ) & 0xFF;
				b = (rgb) & 0xFF;
				
				if (a == 255) {
					
					//Blocks
					if (r == 0 && g == 0 && b == 0) {
						blocks[x][y] = BLOCK.DARK_GRASS;
					} else if (r == 76 && g == 92 && b == 59) {
						blocks[x][y] = BLOCK.DARKENING_GRASS;
					} else if (r == 160 && g == 176 && b == 143) {
						blocks[x][y] = BLOCK.GRASS;
					} else {
						blocks[x][y] = BLOCK.NONE;
					}
					
					//Enemies
					if (r == 0 && g == 174 & b == 255) {
						entities.add(new Bat(realX, realY, this));
					} else if (r == 126 && g == 255 & b == 0) {
						entities.add(new Skeleton(realX, realY, this));
					} else if (r == 87 && g == 176 & b == 0) {
						entities.add(new SkeletonGrunt(realX, realY, this));
					} else if (r == 0 && g == 94 & b == 138) {
						entities.add(new FastBat(realX, realY, this));
					} else if (r == 255 && g == 132 & b == 117) {
						entities.add(new Mage(realX, realY, this));
					} else if (r == 255 && g == 255 & b == 61) {
						entities.add(new DeadlySkeleton(realX, realY, this));
					} else if (r == 204 && g == 0 & b == 255) {
						entities.add(new FloatingMage(realX, realY, this));
					} else if (r == 205 && g == 53 & b == 196) {
						entities.add(new Boss(realX, realY, this));
					}
					
					//Spawn Point
					if (r == 255 && g == 251 && b == 179) {
						player.x = realX;
						player.y = realY;
					}
					
					//Powerups
					if (r == 255 && g == 0 && b == 0) {
						entities.add(new HealthPowerup(realX, realY, this));
					} else if (r == 179 && g == 252 && b == 255) {
						entities.add(new GravityPowerup(realX, realY, this));
					} else if (r == 255 && g == 179 && b == 225) {
						entities.add(new BulletNumberPowerup(realX, realY, this));
					} else if (r == 255 && g == 216 & b == 0) {
						entities.add(new KnockbackPowerup(realX, realY, this));
					} else if (r == 178 && g == 163 & b == 0) {
						entities.add(new FireRatePowerup(realX, realY, this));
					} else if (r == 91 && g == 138 & b == 0) {
						entities.add(new ProjectileSpeedPowerup(realX, realY, this));
					}
					
				} else {
					blocks[x][y] = BLOCK.NONE;
				}
				
			}
		}
		
		int v1, v2, v3, v4;
		float texX, texY;
		
		for (int i = 0; i < width / SEGMENT_SIZE; i++) {
			
			meshes[i] = Main.renderer.beginMesh();
			
			for (int x = i * SEGMENT_SIZE; x < i * SEGMENT_SIZE + SEGMENT_SIZE; x++) {
				for (int y = 0; y < height; y++) {
					
					texX = (blocks[x][y].id % 4) / 4.0f;
					texY = 1.0f - (blocks[x][y].id / 4) / 4.0f - .25f;
					
					v1 = Main.renderer.addVertexToMesh(new Vector3f(0.0f + x * 8.0f, 0.0f + y * 8.0f, -1.0f), new Vector2f(texX, texY));
					v2 = Main.renderer.addVertexToMesh(new Vector3f(8.0f + x * 8.0f, 0.0f + y * 8.0f, -1.0f), new Vector2f(texX + 0.25f, texY));
					v3 = Main.renderer.addVertexToMesh(new Vector3f(8.0f + x * 8.0f, 8.0f + y * 8.0f, -1.0f), new Vector2f(texX + 0.25f, texY + 0.25f));
					v4 = Main.renderer.addVertexToMesh(new Vector3f(0.0f + x * 8.0f, 8.0f + y * 8.0f, -1.0f), new Vector2f(texX, texY + 0.25f));
					
					Main.renderer.addTriangleToMesh(v1, v2, v3);
					Main.renderer.addTriangleToMesh(v1, v3, v4);
					
				}
			}
			
			Main.renderer.finishMesh();
			
		}
		
	}
	
	public void update() {

		for (Entity entity : toAdd) {
			entities.add(entity);
		}
		
		toAdd.clear();
		
		for (Entity enemy : entities) {
			enemy.update();
			if (enemy.destroy) {
				toRemove.add(enemy);
			}
		}

		for (Entity enemy : toRemove) {
			enemy.delete();
			entities.remove(enemy);
		}
		
		toRemove.clear();
		
		Projectile.updateProjectiles();
		DeathAnimation.updateAnimations();
		
		player.update();
		
	}
	
	public void render(double delta) {
		
		float actualX = (float) ((player.x * delta) + (player.lastX * (1.0 - delta)));
		float actualY = (float) ((player.y * delta) + (player.lastY * (1.0 - delta)));

		if (!Main.interpolation) {
			actualX = Math.round(actualX);
			actualY = Math.round(actualY);
		}
		
		float cameraX = 0;
		float cameraY = 0;
		
		if (actualX > Main.WIDTH / 2 - 4) {
			cameraX = actualX - Main.WIDTH / 2 + 4;
		}
		
		if (actualY > Main.HEIGHT / 2 - 4) {
			cameraY = actualY - Main.HEIGHT / 2 + 12;
		}
		
		Main.camera.setPosition(new Vector3f(cameraX, cameraY, 0));
		
		for (int i = (int) (actualX / 8 / SEGMENT_SIZE) - 1; i < (int) (actualX / 8 / SEGMENT_SIZE) + 2; i++) {
			
			i = (int) OtherMath.clamp(i, 0, width / SEGMENT_SIZE);
			
			Matrix4f output = Main.camera.orthoMatrix();
			Matrix4f.translate(new Vector3f(0, 0, -5f), output, output);
			Main.renderer.setMatrix(output);
			Main.renderer.bindTexture(blockTextures, 0);
			Main.renderer.drawMesh(meshes[i]);
			
		}
		
		Projectile.renderProjectiles(delta);
		DeathAnimation.renderAnimations(delta);
		
		player.render(delta);

		for (Entity enemy : entities) {
			enemy.render(delta);
		}
		
	}
	
	public void renderUI() {
		
		player.renderUI();
		
		if (textBrightness >= 1 && System.currentTimeMillis() - lastTextUpdate >= 750) {
			textBrightness--;
			lastTextUpdate = System.currentTimeMillis();
		}

		if (textBrightness >= 1) {
			
			Main.renderer.bindTexture(textBackTexture, 0);
				
			Matrix4f output = Main.camera.orthoMatrix();
			Matrix4f.translate(new Vector3f(0, 0, 0.15f), output, output);
			Main.renderer.setMatrix(output);
			
			Main.renderer.drawMesh(textBack);
		
			Main.font.render(0, 1, 1, 0.25f, text, textBrightness);
		}
		
	}
	
	public void updateText(String txt) {
		
		Main.renderer.deleteMesh(text);
		textBrightness = 4;
		text = Main.font.createVBOFromString(txt);
		
	}
	
	public int getHighestY(int blockX, int blockY) {

		for (int y = blockY; y > 0; y--) {
			if (blocks[blockX][blockY] != BLOCK.NONE) {
				return y;
			}
		}
		
		return 0;
		
	}
	
}