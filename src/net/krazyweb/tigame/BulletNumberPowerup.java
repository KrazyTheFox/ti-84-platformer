package net.krazyweb.tigame;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

public class BulletNumberPowerup extends Powerup {
	
	private static int powerupTexture = 0;
	
	public BulletNumberPowerup(float x, float y, Level level) {
		super(x, y, level);
		if (powerupTexture == 0) {
			powerupTexture = Main.renderer.loadTexture("bulletNumberPowerup.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		}
		texture = powerupTexture;
	}

	@Override
	public void performPowerup() {
		level.player.numberOfProjectiles += 1;
		level.updateText("More Arrows!");
	}
	
}
