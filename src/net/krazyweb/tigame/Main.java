package net.krazyweb.tigame;

import net.krazyweb.input.Input;
import net.krazyweb.opengl.OpenGL;
import net.krazyweb.opengl.OpenGLCamera;
import net.krazyweb.renderer.Camera;
import net.krazyweb.renderer.Color;
import net.krazyweb.renderer.Renderer;
import net.krazyweb.renderer.Renderer.BLEND_MODE;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Main {
	
	public static final int GAME_SCALE = 8; 
	public static final int WIDTH = 96;
	public static final int HEIGHT = 64;
	
	private static final int ACCUMULATOR_UPDATE = 16;
	
	protected static boolean interpolation = false;
	
	private static boolean gameLost = false;
	
	private static boolean menuOpen = true;
	private static int[] menuText = new int[5];
	
	public static Renderer renderer;
	public static Camera camera, uiCamera, gameCamera;
	public static Font font;
	
	private boolean closeRequested = false;
	
	private int shader;
	private Level level;
	private int menuBackground;
	private int menuBackgroundTexture;
	private long menuCloseTime = 0;
	
	public Main() {
		
		/**
		 * TODO: Add credits screen! (Add Sala as tester)
		 */
		
		renderer = new OpenGL();
		
		renderer.createContext(WIDTH * GAME_SCALE, HEIGHT * GAME_SCALE);
		renderer.setClearColor(new Color(239, 255, 222, 255));
		renderer.enableBlending(BLEND_MODE.TRANSPARENCY);
		
		shader = renderer.createShader("diffuse.vert", "diffuse.frag");
		gameCamera = camera = new OpenGLCamera(new Vector3f(0,0,0), WIDTH, HEIGHT, 100f, -10f, 10f);
		uiCamera = new OpenGLCamera(new Vector3f(0,0,0), WIDTH, HEIGHT, 100f, -10f, 10f);

		font = new Font("pixelmix.fnt", renderer);
		
		drawLoadingScreen();
		
		menuBackground = renderer.getQuad(0, 0, 0, WIDTH, HEIGHT);
		menuBackgroundTexture = renderer.loadTexture("blackTexture.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		
		level = new Level(renderer.loadTexture("blocks.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST));
		
		menuText[0] = font.createVBOFromString("TI-84 Game");
		menuText[1] = font.createVBOFromString("Mitchell Matthews");
		menuText[2] = font.createVBOFromString("Sprites by Oryx:");
		menuText[3] = font.createVBOFromString("oryxdesignlab.com");
		menuText[4] = font.createVBOFromString("Space to Play");
		
		double currentTime = getTime();
		double accumulator = 0;
		
		while (!closeRequested && !renderer.isClosed()) {
			
			if (menuOpen) {
					
				renderer.clear();
				renderer.useShader(shader);
				renderer.setShaderProperty("texture", 0);
				renderer.setShaderProperty("diffuseColor", Color.WHITE);
				
				Matrix4f output = Main.camera.orthoMatrix();
				Matrix4f.translate(new Vector3f(0f, 0f, 0f), output, output);
				Main.renderer.setMatrix(output);
				
				Main.renderer.bindTexture(menuBackgroundTexture, 0);
				
				Main.renderer.drawMesh(menuBackground);
				
				int brightness = 3;
				
				for (int i = 0; i < menuText.length; i++) {
					if (i == 0 || i == 4) {
						brightness = 3;
					} else if (i == 1) {
						brightness = 2;
					} else {
						brightness = 1;
					}
					font.render(0, 1, HEIGHT - 11 - 12 * i, 0, menuText[i], brightness);
				}
				
				if (Input.isKeyPressed(Keyboard.KEY_SPACE)) {
					if (gameLost) {
						closeRequested = true;
					} else {
						menuOpen = false;
						menuCloseTime = System.currentTimeMillis(); 
					}
				}
				
			} else if (System.currentTimeMillis() - menuCloseTime > 250) {
				
				if (Input.isKeyPressed(Keyboard.KEY_I)) {
					interpolation = !interpolation;
					if (interpolation) {
						level.updateText("Smooth Movement");
					} else {
						level.updateText("Pixel Movement");
					}
				}
			
				double newTime = getTime();
				double frameTime = newTime - currentTime;
				
				if (frameTime > 25f) {
					frameTime = 25f;
				}
				
				currentTime = newTime;
				
				accumulator += frameTime;
				
				while (accumulator >= ACCUMULATOR_UPDATE) {
					
					update();
					accumulator -= ACCUMULATOR_UPDATE;
					
				}
				
				render(accumulator / ACCUMULATOR_UPDATE);
				renderUI();
			
			}
			
			renderer.syncFrames(120);
			renderer.update();
			
		}
		
		
	}
	
	private void update() {
		level.update();
	}
	
	private void render(double delta) {
		
		camera = gameCamera;
		
		renderer.clear();
		renderer.useShader(shader);
		renderer.setShaderProperty("texture", 0);
		renderer.setShaderProperty("diffuseColor", Color.WHITE);
		
		level.render(delta);
		
	}
	
	private void renderUI() {
		
		camera = uiCamera;
		renderer.setShaderProperty("diffuseColor", Color.WHITE);
		
		level.renderUI();
		
	}
	
	private void drawLoadingScreen() {
		
		renderer.clear();
		renderer.useShader(shader);
		renderer.setShaderProperty("diffuseColor", Color.WHITE);
		
		font.render(0, 1, 1, 0, font.createVBOFromString("Loading..."), 2);
		
		Display.update();
		
	}
	
	public static void endGame(boolean win) {
		
		if (!win) {
			
			gameLost = true;
			menuOpen = true;
			
			font.deleteFBO(menuText[0]);
			font.deleteFBO(menuText[1]);
			font.deleteFBO(menuText[2]);
			font.deleteFBO(menuText[3]);
			font.deleteFBO(menuText[4]);
			
			int blankLine = font.createVBOFromString(" ");
			
			menuText[0] = blankLine;
			menuText[1] = blankLine;
			menuText[2] = blankLine;
			menuText[3] = blankLine;
			menuText[4] = font.createVBOFromString("Game Over");
			
		} else {

			gameLost = true;
			menuOpen = true;
			
			font.deleteFBO(menuText[0]);
			font.deleteFBO(menuText[1]);
			font.deleteFBO(menuText[2]);
			font.deleteFBO(menuText[3]);
			font.deleteFBO(menuText[4]);
			
			int blankLine = font.createVBOFromString(" ");
			
			menuText[0] = blankLine;
			menuText[1] = font.createVBOFromString("Congratulations!");
			menuText[2] = blankLine;
			menuText[3] = font.createVBOFromString("You win!");
			menuText[4] = blankLine;
			
		}
		
	}
	
	public static long getTime() {
		return System.nanoTime() / 1000000;
	}
	
	public static void main(String[] args) {
		new Main();
	}

}
