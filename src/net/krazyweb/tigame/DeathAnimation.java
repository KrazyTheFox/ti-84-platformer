package net.krazyweb.tigame;

import java.util.ArrayList;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

public class DeathAnimation extends Entity {
	
	private static final int ANIMATION_RATE = 40;

	private static ArrayList<DeathAnimation> animations = new ArrayList<DeathAnimation>();
	
	private static int textures[] = new int[5];
	
	private static boolean loaded = false;

	private long lastFrame = 0;
	private int currentFrame = 0;
	
	public DeathAnimation(float x, float y, Level level) {
		
		super(x, y, level);
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("deathAnimation1.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("deathAnimation2.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[2] = Main.renderer.loadTexture("deathAnimation3.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[3] = Main.renderer.loadTexture("deathAnimation4.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[4] = Main.renderer.loadTexture("deathAnimation5.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		}
		
		texture = textures[0];
		lastFrame = System.currentTimeMillis();
		
		animations.add(this);
		
	}
	
	@Override
	public void update() {
		
		if (System.currentTimeMillis() - lastFrame > ANIMATION_RATE) {
			if (currentFrame < textures.length - 1) {
				texture = textures[++currentFrame];
				lastFrame = System.currentTimeMillis();
			} else {
				destroy = true;
			}
		}
		
	}
	
	public static void updateAnimations() {
		
		ArrayList<DeathAnimation> toRemove = new ArrayList<DeathAnimation>();
		
		for (DeathAnimation animation : animations) {
			animation.update();
			if (animation.destroy) {
				toRemove.add(animation);
			}
		}
		
		for (DeathAnimation animation : toRemove) {
			animation.delete();
			animations.remove(animation);
		}
		
		toRemove.clear();
		
	}
	
	public static void renderAnimations(double delta) {

		for (DeathAnimation animation : animations) {
			animation.render(delta);
		}
		
	}
	
}