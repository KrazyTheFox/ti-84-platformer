package net.krazyweb.tigame;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector2f;

public class Projectile extends Entity {
	
	private static ArrayList<Projectile> projectiles = new ArrayList<Projectile>();
	private static ArrayList<Projectile> toRemove = new ArrayList<Projectile>();
	
	private Vector2f direction;
	
	private boolean friendly = false;
	
	protected Vector2f originalLocation;
	protected float moveSpeed = 1.25f;
	protected float knockback = 2f;
	
	protected boolean collidesWithTerrain = true;
	
	public Projectile(int texture, float x, float y, Level level, boolean friendly) {
		super(x, y, level);
		this.texture = texture;
		this.friendly = friendly;
		projectiles.add(this);
	}
	
	@Override
	public void update() {
		
		super.update();
		
		x += moveSpeed * direction.x;
		y += moveSpeed * direction.y;
		
		if (collidesWithTerrain) {
		
			int blockX = (int) ((x) / 8);
			int blockY = (int) ((y) / 8);
			
			for (int ix = -1; ix < 2; ix++) {
				for (int iy = -1; iy < 2; iy++) {
					
					if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {
	
						if (level.blocks[ix + blockX][iy + blockY].id == 15) {
							continue;
						}
						
						int actualBlockX = (blockX + ix) * 8;
						int actualBlockY = (blockY + iy) * 8;
						
						if (x < actualBlockX + 4 && x + 3 > actualBlockX && y + 3 > actualBlockY && y < actualBlockY + 4) {
							destroy = true;
						}
						
					}
					
				}
			}
		
		}
		
		if (friendly) {
			
			for (Entity enemy : level.entities) {

				if (enemy instanceof Powerup) {
					continue;
				}
				
				if (enemy instanceof Boss) {
					
					if (x < enemy.x + 12 && x + 3 > enemy.x && y + 3 > enemy.y && y < enemy.y + 12) {
						
						enemy.damage(1);
						
						if (System.currentTimeMillis() - enemy.lastHit > 50) {
							enemy.xBounce += (originalLocation.x > enemy.x + 8) ? -knockback : knockback;
							enemy.lastHit = System.currentTimeMillis();
						}
						
						destroy = true;
						
						break;
						
					}
					
				} else {
					
					if (x < enemy.x + 4 && x + 3 > enemy.x && y + 3 > enemy.y && y < enemy.y + 4) {
						
						enemy.damage(1);
						
						if (System.currentTimeMillis() - enemy.lastHit > 50) {
							enemy.xBounce += (originalLocation.x > enemy.x + 4) ? -knockback : knockback;
							enemy.lastHit = System.currentTimeMillis();
						}
						
						destroy = true;
						
						break;
						
					}
				
				}
				
			}
			
			for (Entity enemy : Boss.entities) {

				if (enemy instanceof Powerup) {
					continue;
				}
				
				if (x < enemy.x + 4 && x + 3 > enemy.x && y + 3 > enemy.y && y < enemy.y + 4) {
					
					enemy.damage(10);
					
					if (System.currentTimeMillis() - enemy.lastHit > 50) {
						enemy.xBounce += (originalLocation.x > enemy.x + 4) ? -knockback : knockback;
						enemy.lastHit = System.currentTimeMillis();
					}
					
					destroy = true;
					
					break;
					
				}
				
			}
			
		} else {
			
			if (x < level.player.x + 4 && x + 3 > level.player.x && y + 3 > level.player.y && y < level.player.y + 4) {
				level.player.damage(1);
				destroy = true;
			}
			
		}
		
	}
	
	public static void updateProjectiles() {
		
		for (Projectile proj : projectiles) {
			proj.update();
			Vector2f lVec = new Vector2f();
			Vector2f.sub(new Vector2f(proj.x, proj.y), proj.originalLocation, lVec);
			if (proj.destroy || lVec.length() > 125f) {
				toRemove.add(proj);
			}
		}

		for (Projectile proj : toRemove) {
			proj.delete();
			projectiles.remove(proj);
		}
		
		toRemove.clear();
		
	}
	
	public static void renderProjectiles(double delta) {

		for (Projectile proj : projectiles) {
			proj.render(delta);
		}
		
	}
	
	public void setDirection(Vector2f direction) {
		this.direction = direction;
	}

}
