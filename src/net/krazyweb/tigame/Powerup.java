package net.krazyweb.tigame;

public class Powerup extends Entity {
	
	public Powerup(float x, float y, Level level) {
		super(x, y, level);
	}

	@Override
	public void update() {
		if (x - 6 < level.player.x + 4 && x + 6 > level.player.x && y + 6 > level.player.y && y - 6 < level.player.y + 4) {
			performPowerup();
			destroy = true;
		}
	}
	
	public void performPowerup() {
		
	}
	
}