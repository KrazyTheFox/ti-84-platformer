package net.krazyweb.tigame;

import net.krazyweb.input.Input;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Player extends Entity {
	
	protected float moveSpeed = 0.75f;
	protected float projectileSpeedBoost = 0.0f;
	protected float projectileKnockback = 0f;
	protected float numberOfProjectiles = 1;
	protected float gravity = .2f;
	
	protected boolean invincible = false;
	protected long lastDamage = 0;
	protected long lastInvincibleAnimation = 0;
	protected boolean invincibleTextureActive = false;

	protected float xVelocity = 0;
	protected float yVelocity = 0;
	protected float yBounce = 0;
	
	protected float knockbackAmount = 3f;
	
	protected int fireRate = 750;
	
	private int facingRightInvincibleTexture = 0;
	private int facingLeftInvincibleTexture = 0;
	
	private int playerProjectile;
	private long lastClick = 0;
	
	private int playerHealthMesh;
	private int playerHealthTexture;
	private int playerHealthTextureEmpty;
	
	private float jumpFuel = 0f;
	private float jumpFuelMax = 1.75f;
	private float jumpRate = 0.25f;
	private float jumpStrengthInitial = 0.90f;
	private float jumpStrength = 0.25f;
	private boolean jumping = false;
	private boolean touchingGround = false;
	
	private int facingRightTexture = 0;
	private int facingLeftTexture = 0;
	private boolean facingRight = true;
	
	public Player(float x, float y, Level level) {
		
		super(x, y, level);
		
		facingRightTexture = Main.renderer.loadTexture("character.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		facingLeftTexture = Main.renderer.loadTexture("characterLeft.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		
		facingRightInvincibleTexture = Main.renderer.loadTexture("characterInvincible.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		facingLeftInvincibleTexture = Main.renderer.loadTexture("characterLeftInvincible.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		
		playerProjectile = Main.renderer.loadTexture("projectileSmall.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		playerHealthTexture = Main.renderer.loadTexture("health.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		playerHealthTextureEmpty = Main.renderer.loadTexture("healthEmpty.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		
		playerHealthMesh = Main.renderer.beginMesh();
		
		int v1 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0625f, 0.0625f));
		int v2 = Main.renderer.addVertexToMesh(new Vector3f(7.0f, 0.0f, 0.0f), new Vector2f(0.5f, 0.0625f));
		int v3 = Main.renderer.addVertexToMesh(new Vector3f(7.0f, 7.0f, 0.0f), new Vector2f(0.5f, 0.5f));
		int v4 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 7.0f, 0.0f), new Vector2f(0.0625f, 0.5f));
		
		Main.renderer.addTriangleToMesh(v1, v2, v3);
		Main.renderer.addTriangleToMesh(v1, v3, v4);
		
		Main.renderer.finishMesh();
		
		health = maxHealth = 2;
		
	}
	
	@Override
	public void update() {
		
		super.update();
		
		int mouseX = Input.getMouseX() / Main.GAME_SCALE;

		if (x > Main.WIDTH / 2) {
			mouseX += x - Main.WIDTH / 2;
		}
		
		facingRight = (mouseX > x);
		
		if (invincible && System.currentTimeMillis() - lastDamage > 1000) {
			invincible = false;
		}
		
		if (facingRight) {
			
			if (invincible && System.currentTimeMillis() - lastInvincibleAnimation > 125) {
				if (invincibleTextureActive) {
					texture = facingRightTexture;
					invincibleTextureActive = false;
				} else {
					texture = facingRightInvincibleTexture;
					invincibleTextureActive = true;
				}
				lastInvincibleAnimation = System.currentTimeMillis();
			} else if (!invincible) {
				texture = facingRightTexture;
			}
			
		} else {
			
			if (invincible && System.currentTimeMillis() - lastInvincibleAnimation > 125) {
				if (invincibleTextureActive) {
					texture = facingLeftTexture;
					invincibleTextureActive = false;
				} else {
					texture = facingLeftInvincibleTexture;
					invincibleTextureActive = true;
				}
				lastInvincibleAnimation = System.currentTimeMillis();
			} else if (!invincible) {
				texture = facingLeftTexture;
			}
			
		}
		
		xVelocity = 0;
		
		if (Input.isKeyDown(Keyboard.KEY_D)) {
			xVelocity = moveSpeed;
		}
		
		if (Input.isKeyDown(Keyboard.KEY_A)) {
			xVelocity = -moveSpeed;
		}
		
		if (touchingGround) {
			jumpFuel = jumpFuelMax;
			jumping = false;
		}
		
		if (!Input.isKeyDown(Keyboard.KEY_SPACE) && jumpFuel > 0 && jumping) {
			jumpFuel = 0;
			jumping = false;
		}
		
		if (Input.isKeyDown(Keyboard.KEY_SPACE) && jumpFuel > 0) {
			if (!jumping) {
				yVelocity += jumpStrengthInitial;
				jumping = true;
			} else {
				jumpFuel -= jumpRate;
				yVelocity += jumpStrength;
			}
		} else {
			yVelocity -= gravity;
		}
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		touchingGround = false;
		
		if (xVelocity == 0) {
			x = Math.round(x);
		}
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x + xVelocity + xBounce < actualBlockX + 8 && x + xVelocity + xBounce + 8 > actualBlockX && y + 8 > actualBlockY && y < actualBlockY + 8) {
						xVelocity = 0;
						xBounce = 0;
						if (x > actualBlockX) {
							x = actualBlockX + 8;
						} else if (x < actualBlockX) {
							x = actualBlockX - 8;
						}
					}
					
					if (x < actualBlockX + 8 && x + 8 > actualBlockX && y + yVelocity + 8 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						if (y > actualBlockY) {
							touchingGround = true;
							y = actualBlockY + 8;
						} else if (y < actualBlockY) {
							y = actualBlockY - 8;
						}
						yVelocity = 0;
					}
					
				}
				
			}
		}
		
		x += xVelocity + xBounce;
		y += yVelocity;

		if (x < 0) {
			x = 0;
		}
		
		if (y < 0) {
			y = 0;
		}
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - gravity : xBounce + gravity;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (Input.isMouseClicked(Input.MOUSE.LEFT) && System.currentTimeMillis() - lastClick > fireRate) {
			
			Vector2f mousePos = new Vector2f((float) (Input.getMouseX() + 1) / Main.GAME_SCALE, (float) (Input.getMouseY() + 1) / Main.GAME_SCALE - 4);
			Vector2f pos = (facingRight) ? new Vector2f(x + 5, y) : new Vector2f(x - 5, y);
			Vector2f dir = new Vector2f();
			
			if (x > Main.WIDTH / 2) {
				mousePos.x += x - Main.WIDTH / 2;
			}
			
			if (y > Main.HEIGHT / 2) {
				mousePos.y += y - Main.HEIGHT / 2 + 12;
			}
			
			Vector2f.sub(mousePos, pos, dir);
			
			if (dir.length() <= 0) {
				mousePos.x += 1;
				Vector2f.sub(mousePos, pos, dir);
			}
			
			int spread = 1;
			
			for (int i = 0; i < numberOfProjectiles; i++) {
				
				double angle = Math.toDegrees(Math.atan2(dir.y, dir.x));
				
				if (numberOfProjectiles > 1) {
					if (numberOfProjectiles % 2 == 1) {
						if (i > 0) {
							angle += ((i - 1) % 2 == 1) ? -5 * spread : 5 * spread;
							if (i % 2 == 0) {
								spread++;
							}
						}
					} else {
						angle += (i % 2 == 1) ? -5 * spread : 5 * spread;
						if (i % 2 == 1) {
							spread++;
						}
					}
				}
				
				angle = Math.toRadians(angle);
				
				Vector2f newDir = new Vector2f((float) Math.cos(angle), (float) Math.sin(angle));
				
				Projectile proj = new Projectile(playerProjectile, (facingRight) ? x + 5 : x - 5, y, level, true);
				
				proj.moveSpeed += projectileSpeedBoost;
				proj.knockback += projectileKnockback;
				proj.setDirection(newDir);
				proj.originalLocation = pos;
			
			}
			
			lastClick = System.currentTimeMillis();
			
		}
		
	}
	
	@Override
	public void renderUI() {
		
		if (Boss.hostile) {
			
			Main.renderer.bindTexture(Boss.healthTex, 0);
			
			for (int i = 0; i < (Boss.displayHealth / 10); i++) {
				
				Matrix4f output = Main.camera.orthoMatrix();
				Matrix4f.translate(new Vector2f(18 + 2 * i, 0), output, output);
				Main.renderer.setMatrix(output);
				
				Main.renderer.drawMesh(Boss.healthMesh);
				
			}
			
		}
		
		Main.renderer.bindTexture(playerHealthTextureEmpty, 0);
		
		for (int i = 0; i < maxHealth; i++) {
			
			Matrix4f output = Main.camera.orthoMatrix();
			Matrix4f.translate(new Vector2f(0 + 6 * i, Main.HEIGHT - 7), output, output);
			Main.renderer.setMatrix(output);
			
			Main.renderer.drawMesh(playerHealthMesh);
			
		}
		
		Main.renderer.bindTexture(playerHealthTexture, 0);
		
		for (int i = 0; i < health; i++) {
			
			Matrix4f output = Main.camera.orthoMatrix();
			Matrix4f.translate(new Vector2f(0 + 6 * i, Main.HEIGHT - 7), output, output);
			Main.renderer.setMatrix(output);
			
			Main.renderer.drawMesh(playerHealthMesh);
			
		}
		
	}
	
	@Override
	public void damage(int damage) {
		
		if (damage <= 0 || invincible) {
			return;
		}
		
		invincible = true;
		
		health -= damage;
		
		if (health <= 0) {
			health = 0;
			Main.endGame(false);
		}
		
		lastDamage = lastInvincibleAnimation = System.currentTimeMillis();
		invincibleTextureActive = false;
		
	}
	
}
