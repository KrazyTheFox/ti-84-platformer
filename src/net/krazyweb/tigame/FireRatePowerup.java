package net.krazyweb.tigame;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

public class FireRatePowerup extends Powerup {
	
	private static int powerupTexture = 0;
	
	public FireRatePowerup(float x, float y, Level level) {
		super(x, y, level);
		if (powerupTexture == 0) {
			powerupTexture = Main.renderer.loadTexture("bulletNumberPowerup.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
		}
		texture = powerupTexture;
	}

	@Override
	public void performPowerup() {
		level.player.fireRate -= 100;
		level.updateText("Faster Fire Rate!");
	}
	
}
