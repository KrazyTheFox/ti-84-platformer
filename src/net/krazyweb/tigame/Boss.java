package net.krazyweb.tigame;

import java.util.ArrayList;

import net.krazyweb.renderer.OtherMath;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Boss extends Entity {
	
	private static int[] textures = new int[6];
	private static int projectile;
	private static boolean loaded = false;
	
	protected static int healthMesh;
	protected static int healthTex;
	protected static int displayHealth;

	protected static boolean hostile = false;
	
	protected static ArrayList<Entity> entities = new ArrayList<Entity>();
	protected static ArrayList<Entity> toRemove = new ArrayList<Entity>();
	
	private int state = 0;
	private int nextState = 0;
	private long lastStateChange = 0;
	private int stateTime = 0;
	private boolean stateTimeSet = false;
	private boolean enemiesSpawned = false;
	
	private long lastFire = 0;
	private long chargeStartTime = 0;
	private boolean charging = false;
	private long lastChargingAnimation = 0;
	private boolean chargingTextureActive = false;

	private float xVelocity = 0;
	private float yVelocity = 0;
	
	private boolean touchingGround = false;
	private float groundBounce = 0;
	
	private boolean invincible = false;
	
	public Boss(float x, float y, Level level) {
		
		super(x, y, level);
		
		Main.renderer.deleteMesh(mesh);
		
		mesh = Main.renderer.beginMesh();
		
		int v1 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.25f, 0.25f));
		int v2 = Main.renderer.addVertexToMesh(new Vector3f(16.0f, 0.0f, 0.0f), new Vector2f(0.75f, 0.25f));
		int v3 = Main.renderer.addVertexToMesh(new Vector3f(16.0f, 16.0f, 0.0f), new Vector2f(0.75f, 0.75f));
		int v4 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 16.0f, 0.0f), new Vector2f(0.25f, 0.75f));
		
		Main.renderer.addTriangleToMesh(v1, v2, v3);
		Main.renderer.addTriangleToMesh(v1, v3, v4);
		
		Main.renderer.finishMesh();
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("bossLeft.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("bossRight.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[2] = Main.renderer.loadTexture("bossLeftCasting.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[3] = Main.renderer.loadTexture("bossRightCasting.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[4] = Main.renderer.loadTexture("bossLeftInvincible.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[5] = Main.renderer.loadTexture("bossRightInvincible.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			projectile = Main.renderer.loadTexture("projectileSmall.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			
			healthTex = Main.renderer.loadTexture("bossHealth.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			
			healthMesh = Main.renderer.beginMesh();
			
			v1 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 0.0f));
			v2 = Main.renderer.addVertexToMesh(new Vector3f(2.0f, 0.0f, 0.0f), new Vector2f(0.25f, 0.0f));
			v3 = Main.renderer.addVertexToMesh(new Vector3f(2.0f, 2.0f, 0.0f), new Vector2f(0.25f, 0.25f));
			v4 = Main.renderer.addVertexToMesh(new Vector3f(0.0f, 2.0f, 0.0f), new Vector2f(0.0f, 0.25f));
			
			Main.renderer.addTriangleToMesh(v1, v2, v3);
			Main.renderer.addTriangleToMesh(v1, v3, v4);
			
			Main.renderer.finishMesh();
			
			loaded = true;
		}
		
		health = maxHealth = 300;
		
		texture = textures[0];
		
		lastStateChange = System.currentTimeMillis();
		
	}
	
	@Override
	public void update() {
		
		if (!hostile && x - level.player.x < 250) {
			hostile = true;
		} else if (!hostile) {
			return;
		}
		
		displayHealth = health;
		
		super.update();
		
		switch (state) {
			case 0:
				moveAndAttack();
				break;
			case 1:
				summonSkeletons();
				break;
			case 2:
				summonBats();
				break;
			case 3:
				summonMages();
				break;
		}
		
		if (System.currentTimeMillis() - lastStateChange > stateTime) {
			
			lastStateChange = System.currentTimeMillis();
			state = nextState;
			stateTimeSet = false;
			enemiesSpawned = false;
			
		}
		
		for (Entity enemy : entities) {
			enemy.update();
			if (enemy.destroy) {
				toRemove.add(enemy);
			}
		}

		for (Entity enemy : toRemove) {
			enemy.delete();
			entities.remove(enemy);
		}
		
		toRemove.clear();
		
	}
	
	@Override
	public void render(double delta) {
		
		super.render(delta);

		for (Entity enemy : entities) {
			enemy.render(delta);
		}
		
	}
	
	private void moveAndAttack() {
		
		invincible = false;
		
		nextState = 1 + (int)(Math.random() * ((3 - 1) + 1));
		
		if (!stateTimeSet) {
			stateTime = 5000 + (int)(Math.random() * ((10000 - 5000) + 1));
			stateTimeSet = true;
		}
		
		Vector2f moveDir = new Vector2f(level.player.x, level.player.y + 8);
		Vector2f.sub(moveDir, new Vector2f(x, y), moveDir);
		float distance = moveDir.length();
		
		if (moveDir.length() > 0) {
			moveDir.normalise();
		}
		
		if (charging && System.currentTimeMillis() - chargeStartTime > 700) {
			
			charging = false;
				
			Vector2f mousePos = new Vector2f(level.player.x, level.player.y);
			Vector2f pos = new Vector2f(x, y);
			Vector2f dir = new Vector2f();
			
			Vector2f.sub(mousePos, pos, dir);
			
			if (dir.length() <= 0) {
				mousePos.x += 1;
				Vector2f.sub(mousePos, pos, dir);
			}
				
			Projectile proj = new Projectile(projectile, x, y, level, false);
			
			dir.normalise();
			proj.collidesWithTerrain = false;
			proj.setDirection(dir);
			proj.originalLocation = pos;
			
			lastFire = System.currentTimeMillis();
			
		} else if (!charging && distance > 20 && distance < 40 && System.currentTimeMillis() - lastFire > 1000) {
			
			charging = true;
			chargeStartTime = System.currentTimeMillis();
			
		}
			
		xVelocity += moveDir.x * 0.045f;
		yVelocity += moveDir.y * 0.095f;
		
		xVelocity = OtherMath.clamp(xVelocity, -0.65f, 0.65f);
		yVelocity = OtherMath.clamp(yVelocity, -0.95f, 0.95f);
		
		if (moveDir.x >= 0) {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[3];
					chargingTextureActive = false;
				} else {
					texture = textures[1];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[1];
			}
			
		} else {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[2];
					chargingTextureActive = false;
				} else {
					texture = textures[0];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[0];
			}
			
		}
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x + xVelocity < actualBlockX + 8 && x + xVelocity + 16 > actualBlockX && y + 16 > actualBlockY && y < actualBlockY + 8) {
						
						if (x > actualBlockX) {
							x = actualBlockX + 16;
						} else if (x < actualBlockX) {
							x = actualBlockX - 16;
						}
						
						xVelocity = 0;
						xBounce = 0;
						
					}
					
					if (x < actualBlockX + 8 && x + 16 > actualBlockX && y + yVelocity + 16 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						
						if (y > actualBlockY) {
							if (y + yVelocity <= actualBlockY + 8) {
								y = actualBlockY + 16;
							}
						}
						
						touchingGround = true;
						yVelocity = 0;
						
						if (y < actualBlockY) {
							groundBounce = 0;
						}
						
					}
					
				}
				
			}
			
		}
		
		if (touchingGround) {
			groundBounce = 0.1f;
			touchingGround = false;
		}
		
		if (groundBounce > 0.65f) {
			groundBounce *= 0.8f;
		} else {
			groundBounce *= 1.1f;
		}
		
		if (groundBounce < 0.01f) {
			groundBounce = 0;
		}
		
		x += xVelocity + xBounce;
		y += yVelocity + groundBounce;
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - 0.2f : xBounce + 0.2f;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (x < level.player.x + 8 && x + 16 > level.player.x && y + 16 > level.player.y && y < level.player.y + 8) {
			if (!level.player.invincible) {
				level.player.xBounce += (x > level.player.x) ? -level.player.knockbackAmount : level.player.knockbackAmount;
			}
			level.player.damage(1);
		}
		
	}

	private void summonSkeletons() {
		summon(0);
	}

	private void summonBats() {
		summon(1);
	}

	private void summonMages() {
		summon(2);
	}
	
	private void summon(int unit) {
		
		invincible = true;
		nextState = 0;
		lastStateChange = System.currentTimeMillis();
		
		if (!charging && entities.isEmpty()) {
			charging = true;
			chargeStartTime = System.currentTimeMillis();
		}

		Vector2f moveDir = new Vector2f(level.player.x, level.player.y + 8);
		Vector2f.sub(moveDir, new Vector2f(x, y), moveDir);
		
		xVelocity = 0;
		yVelocity = 0;
		
		if (y < level.player.y + 18) {
			y += 0.1f;
		} else if (y > level.player.y + 24) {
			y -= 0.1f;
		}
		
		if (moveDir.x >= 0) {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[3];
					chargingTextureActive = false;
				} else {
					texture = textures[1];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[1];
			}
			
		} else {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[2];
					chargingTextureActive = false;
				} else {
					texture = textures[0];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[0];
			}
			
		}
		
		if (!enemiesSpawned && entities.isEmpty() && System.currentTimeMillis() - chargeStartTime > 3000) {
			
			new DeathAnimation(x + 8, y, level);
			new DeathAnimation(x - 8, y, level);
			
			switch (unit) {
				case 0:
					entities.add(new DeadlySkeleton(x + 8, y, level));
					entities.add(new DeadlySkeleton(x - 8, y, level));
					break;
				case 1:
					entities.add(new Bat(x + 4, y + 8, level));
					entities.add(new Bat(x - 4, y + 8, level));
					entities.add(new FastBat(x + 8, y, level));
					entities.add(new FastBat(x - 8, y, level));
					break;
				case 2:
					entities.add(new FloatingMage(x + 4, y + 8, level));
					entities.add(new FloatingMage(x - 4, y + 8, level));
					entities.add(new FloatingMage(x + 8, y, level));
					entities.add(new FloatingMage(x - 8, y, level));
					break;
					
			}
			
			enemiesSpawned = true;
			charging = false;
			
		}
		
		if (enemiesSpawned && !entities.isEmpty()) {
			texture = (moveDir.x < 0) ? textures[4] : textures[5];
		}
		
		if (enemiesSpawned && entities.isEmpty()) {
			enemiesSpawned = false;
			level.toAdd.add(new HealthPowerup(x + 4, (level.getHighestY((int) (x / 8), (int) (y / 8)) + 1) * 8 * 8, level));
			System.out.println((level.getHighestY((int) (x / 8), (int) (y / 8)) + 1) * 8 * 8 + ", " + level.player.y);
			lastStateChange = 0;
		}
		
	}
	
	@Override
	public void damage(int damage) {
		
		if (!invincible) {
			health -= damage;
			
			if (health <= 0) {
				health = 0;
				Main.endGame(true);
			}
		}
		
	}
	
}
