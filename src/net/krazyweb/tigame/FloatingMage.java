package net.krazyweb.tigame;

import net.krazyweb.renderer.OtherMath;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Vector2f;

public class FloatingMage extends Entity {
	
	private static int[] textures = new int[4];
	private static int projectile;
	private static boolean loaded = false;
	
	private long lastFire = 0;
	private long chargeStartTime = 0;
	private boolean charging = false;
	private long lastChargingAnimation = 0;
	private boolean chargingTextureActive = false;

	private float xVelocity = 0;
	private float yVelocity = 0;
	
	private boolean touchingGround = false;
	private float groundBounce = 0;
	
	public FloatingMage(float x, float y, Level level) {
		
		super(x, y, level);
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("floatingMageLeft.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("floatingMageRight.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[2] = Main.renderer.loadTexture("floatingMageLeftFiring.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[3] = Main.renderer.loadTexture("floatingMageRightFiring.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			projectile = Main.renderer.loadTexture("projectileSmall.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			loaded = true;
		}
		
		health = maxHealth = 8;
		
		texture = textures[0];
		
	}
	
	@Override
	public void update() {
		
		super.update();
		
		Vector2f moveDir = new Vector2f(level.player.x, level.player.y + 8);
		Vector2f.sub(moveDir, new Vector2f(x, y), moveDir);
		float distance = moveDir.length();
		
		if (moveDir.length() > 0) {
			moveDir.normalise();
		}
		
		if (charging && System.currentTimeMillis() - chargeStartTime > 700) {
			
			charging = false;
				
			Vector2f mousePos = new Vector2f(level.player.x, level.player.y);
			Vector2f pos = new Vector2f(x, y);
			Vector2f dir = new Vector2f();
			
			Vector2f.sub(mousePos, pos, dir);
			
			if (dir.length() <= 0) {
				mousePos.x += 1;
				Vector2f.sub(mousePos, pos, dir);
			}
				
			Projectile proj = new Projectile(projectile, x, y, level, false);
			
			dir.normalise();
			proj.collidesWithTerrain = false;
			proj.setDirection(dir);
			proj.originalLocation = pos;
			
			lastFire = System.currentTimeMillis();
			
		} else if (!charging && distance > 20 && distance < 40 && System.currentTimeMillis() - lastFire > 1000) {
			
			charging = true;
			chargeStartTime = System.currentTimeMillis();
			
		}
		
		if (distance < 100) {
			
			xVelocity += moveDir.x * 0.045f;
			yVelocity += moveDir.y * 0.095f;
			
			xVelocity = OtherMath.clamp(xVelocity, -0.65f, 0.65f);
			yVelocity = OtherMath.clamp(yVelocity, -0.95f, 0.95f);
			
		} else {
			
			xVelocity = 0;
			yVelocity = 0;
			
		}
		
		if (moveDir.x >= 0) {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[3];
					chargingTextureActive = false;
				} else {
					texture = textures[1];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[1];
			}
			
		} else {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[2];
					chargingTextureActive = false;
				} else {
					texture = textures[0];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[0];
			}
			
		}
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x + xVelocity < actualBlockX + 8 && x + xVelocity + 8 > actualBlockX && y + 8 > actualBlockY && y < actualBlockY + 8) {
						
						if (x > actualBlockX) {
							x = actualBlockX + 8;
						} else if (x < actualBlockX) {
							x = actualBlockX - 8;
						}
						
						xVelocity = 0;
						xBounce = 0;
						
					}
					
					if (x < actualBlockX + 8 && x + 8 > actualBlockX && y + yVelocity + 8 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						
						if (y > actualBlockY) {
							if (y + yVelocity <= actualBlockY + 8) {
								y = actualBlockY + 8;
							}
						}
						
						touchingGround = true;
						yVelocity = 0;
						
						if (y < actualBlockY) {
							groundBounce = 0;
						}
						
					}
					
				}
				
			}
		}
		
		if (touchingGround) {
			groundBounce = 0.1f;
			touchingGround = false;
		}
		
		if (groundBounce > 0.65f) {
			groundBounce *= 0.8f;
		} else {
			groundBounce *= 1.1f;
		}
		
		if (groundBounce < 0.01f) {
			groundBounce = 0;
		}
		
		x += xVelocity + xBounce;
		y += yVelocity + groundBounce;
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - 0.2f : xBounce + 0.2f;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (x < level.player.x + 8 && x + 8 > level.player.x && y + 8 > level.player.y && y < level.player.y + 8) {
			if (!level.player.invincible) {
				level.player.xBounce += (x > level.player.x) ? -level.player.knockbackAmount : level.player.knockbackAmount;
			}
			level.player.damage(1);
		}
		
	}
	
}
