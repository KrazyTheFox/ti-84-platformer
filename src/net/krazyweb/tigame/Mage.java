package net.krazyweb.tigame;

import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Vector2f;

public class Mage extends Entity {
	
	private static int[] textures = new int[4];
	private static int projectile;
	private static boolean loaded = false;
	
	private long lastFire = 0;
	private long chargeStartTime = 0;
	private boolean charging = false;
	private long lastChargingAnimation = 0;
	private boolean chargingTextureActive = false;

	private float xVelocity = 0;
	private float yVelocity = 0;
	private float moveSpeed = 0.45f;
	
	
	private boolean touchingGround = false;
	private boolean jumping = false;
	private boolean touchingHorizontal = false;
	
	public Mage(float x, float y, Level level) {
		
		super(x, y, level);
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("mageLeft.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("mageRight.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[2] = Main.renderer.loadTexture("mageLeftFiring.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[3] = Main.renderer.loadTexture("mageRightFiring.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			projectile = Main.renderer.loadTexture("projectileSmall.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			loaded = true;
		}
		
		health = maxHealth = 8;
		
		texture = textures[0];
		
	}
	
	@Override
	public void update() {
		
		super.update();
		
		if (charging && System.currentTimeMillis() - chargeStartTime > 700) {
			
			charging = false;
				
			Vector2f mousePos = new Vector2f(level.player.x, level.player.y);
			Vector2f pos = new Vector2f(x, y);
			Vector2f dir = new Vector2f();
			
			Vector2f.sub(mousePos, pos, dir);
			
			if (dir.length() <= 0) {
				mousePos.x += 1;
				Vector2f.sub(mousePos, pos, dir);
			}
				
			Projectile proj = new Projectile(projectile, x, y, level, false);
			
			dir.normalise();
			proj.collidesWithTerrain = false;
			proj.setDirection(dir);
			proj.originalLocation = pos;
			
			lastFire = System.currentTimeMillis();
			
		} else if (!charging && Math.abs(x - level.player.x) < 38 && System.currentTimeMillis() - lastFire > 1000) {
			
			charging = true;
			chargeStartTime = System.currentTimeMillis();
			
		}
		
		Vector2f dir = new Vector2f(level.player.x, level.player.y + 8);
		Vector2f.sub(dir, new Vector2f(x, y), dir);
		float distance = dir.length();
		
		if (dir.length() > 0) {
			dir.normalise();
		}
		
		if ((distance < 120 && distance > 38) && !charging) {
			
			xVelocity = (dir.x > 0) ? moveSpeed : -moveSpeed;

			if (Math.round(x) == Math.round(level.player.x)) {
				xVelocity = 0;
			}
			
		} else {
			
			xVelocity = 0;
			yVelocity = 0;
			
		}
		
		if (xVelocity >= 0) {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[3];
					chargingTextureActive = false;
				} else {
					texture = textures[1];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[1];
			}
			
		} else {
			
			if (charging && System.currentTimeMillis() - lastChargingAnimation > 75) {
				if (chargingTextureActive) {
					texture = textures[2];
					chargingTextureActive = false;
				} else {
					texture = textures[0];
					chargingTextureActive = true;
				}
				lastChargingAnimation = System.currentTimeMillis();
			} else if (!charging) {
				texture = textures[0];
			}
			
		}
		
		if (touchingGround) {
			jumping = false;
		}
		
		if (touchingHorizontal && !jumping) {
			yVelocity = 0.3f;
			jumping = true;
		} else {
			yVelocity -= 0.2f;
		}
		
		touchingHorizontal = false;
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x + xVelocity < actualBlockX + 8 && x + xVelocity + 8 > actualBlockX && y + 8 > actualBlockY && y < actualBlockY + 8) {
						
						if (x > actualBlockX) {
							x = actualBlockX + 8;
						} else if (x < actualBlockX) {
							x = actualBlockX - 8;
						}
						
						touchingHorizontal = true;
						
						xVelocity = 0;
						xBounce = 0;
						
					}
					
					if (x < actualBlockX + 8 && x + 8 > actualBlockX && y + yVelocity + 8 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						
						if (y > actualBlockY) {
							if (y + yVelocity <= actualBlockY + 8) {
								y = actualBlockY + 8;
							}
						}
						
						touchingGround = true;
						yVelocity = 0;
						
					}
					
				}
				
			}
		}
		
		x += xVelocity + xBounce;
		y += yVelocity;
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - 0.2f : xBounce + 0.2f;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (x < level.player.x + 8 && x + 8 > level.player.x && y + 8 > level.player.y && y < level.player.y + 8) {
			if (!level.player.invincible) {
				level.player.xBounce += (x > level.player.x) ? -level.player.knockbackAmount : level.player.knockbackAmount;
			}
			level.player.damage(1);
		}
		
	}
	
}
