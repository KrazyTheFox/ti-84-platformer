package net.krazyweb.tigame;

import net.krazyweb.renderer.OtherMath;
import net.krazyweb.renderer.Renderer.TEXTURE_FORMAT;
import net.krazyweb.renderer.Renderer.TEXTURE_HINT;

import org.lwjgl.util.vector.Vector2f;

public class Bat extends Entity {
	
	private static final int ANIMATION_RATE = 300;
	
	private static int[] textures = new int[2];
	private static boolean loaded = false;
	
	private long lastAnimation = System.currentTimeMillis();
	private int currentFrame = 0;

	private float xVelocity = 0;
	private float yVelocity = 0;
	private float groundBounce = 0;
	private boolean touchingGround = false;
	
	public Bat(float x, float y, Level level) {
		
		super(x, y, level);
		
		if (!loaded) {
			textures[0] = Main.renderer.loadTexture("bat1.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			textures[1] = Main.renderer.loadTexture("bat2.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			//projectile = Main.renderer.loadTexture("projectileSmall.png", TEXTURE_FORMAT.PNG, TEXTURE_HINT.NEAREST);
			loaded = true;
		}
		
		health = maxHealth = 2;
		
		texture = textures[0];
		
	}
	
	@Override
	public void update() {
		
		super.update();
		
		Vector2f dir = new Vector2f(level.player.x, level.player.y + 7);
		Vector2f.sub(dir, new Vector2f(x, y), dir);
		float distance = dir.length();
		
		if (dir.length() > 0) {
			dir.normalise();
		}
		
		if (distance < 90) {
			
			xVelocity += dir.x * 0.045f;
			yVelocity += dir.y * 0.095f;
			
			xVelocity = OtherMath.clamp(xVelocity, -0.65f, 0.65f);
			yVelocity = OtherMath.clamp(yVelocity, -0.95f, 0.95f);
			
		} else {
			
			xVelocity = 0;
			yVelocity = 0;
			
		}
		
		int blockX = (int) ((x) / 8);
		int blockY = (int) ((y) / 8);
		
		for (int ix = -1; ix < 2; ix++) {
			for (int iy = -1; iy < 2; iy++) {
				
				if (ix + blockX >= 0 && ix + blockX < level.width && iy + blockY >= 0 && iy + blockY < level.height) {

					if (level.blocks[ix + blockX][iy + blockY].id == 15) {
						continue;
					}
					
					int actualBlockX = (blockX + ix) * 8;
					int actualBlockY = (blockY + iy) * 8;
					
					if (x < actualBlockX + 8 && x + 8 > actualBlockX && y + yVelocity + 8 > actualBlockY && y + yVelocity < actualBlockY + 8) {
						
						if (y > actualBlockY) {
							if (y + yVelocity <= actualBlockY + 8) {
								y = actualBlockY + 8;
							}
							touchingGround = true;
						}
						
						yVelocity = 0;
						
						if (y < actualBlockY) {
							groundBounce = 0;
						}
						
					}
					
					if (x + xVelocity< actualBlockX + 8 && x + xVelocity + 8 > actualBlockX && y + 8 > actualBlockY && y < actualBlockY + 8) {
						xVelocity = 0;
						xBounce = 0;
					}
					
				}
				
			}
		}
		
		if (touchingGround) {
			groundBounce = 0.1f;
			touchingGround = false;
		}
		
		if (groundBounce > 0.65f) {
			groundBounce *= 0.8f;
		} else {
			groundBounce *= 1.1f;
		}
		
		if (groundBounce < 0.01f) {
			groundBounce = 0;
		}
		
		x += xVelocity + xBounce;
		y += yVelocity + groundBounce;
		
		float preXBounce = xBounce;
		
		if (xBounce > 0 || xBounce < 0) {
			xBounce = (xBounce > 0) ? xBounce - 0.2f : xBounce + 0.2f;
		}
		
		if ((preXBounce < 0 && xBounce > 0) || (preXBounce > 0 && xBounce < 0)) {
			xBounce = 0;
		}
		
		if (x < level.player.x + 8 && x + 8 > level.player.x && y + 8 > level.player.y && y < level.player.y + 8) {
			if (!level.player.invincible) {
				level.player.xBounce += (x > level.player.x) ? -level.player.knockbackAmount : level.player.knockbackAmount;
			}
			level.player.damage(1);
		}
		
		if (System.currentTimeMillis() - lastAnimation > ANIMATION_RATE) {
			
			if (++currentFrame > textures.length - 1) {
				currentFrame = 0;
			}
			
			texture = textures[currentFrame];
			
			lastAnimation = System.currentTimeMillis();
			
		}
		
	}
	
}
